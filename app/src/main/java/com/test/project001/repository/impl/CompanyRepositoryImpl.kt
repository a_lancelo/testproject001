package com.test.project001.repository.impl

import com.test.project001.models.Company
import com.test.project001.repository.ICompanyRepository
import com.test.project001.repository.api.Api
import io.reactivex.Single

class CompanyRepositoryImpl(
    private val hostApi: Api
) : ICompanyRepository {
    override fun getCompaniesList(): Single<List<Company>> {
        return hostApi.getCompaniesList()
    }

    override fun getCompanyInfo(id: String): Single<List<Company>> {
        return hostApi.getCompanyInfo(id)
    }
}
