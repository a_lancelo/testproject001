package com.test.project001.repository

import com.test.project001.models.Company
import io.reactivex.Single

interface ICompanyRepository {
    fun getCompaniesList(): Single<List<Company>>
    fun getCompanyInfo(id: String): Single<List<Company>>
}
