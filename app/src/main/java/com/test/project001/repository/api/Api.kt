package com.test.project001.repository.api

import com.test.project001.models.Company
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
    @GET("test.php")
    fun getCompaniesList(): Single<List<Company>>
    @GET("test.php")
    fun getCompanyInfo(@Query("id") id: String): Single<List<Company>>
}
