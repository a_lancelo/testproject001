package com.test.project001.ui.main

import com.test.project001.navigation.ICompanyScreens
import moxy.InjectViewState
import moxy.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class MainPresenter(
    private var router: Router,
    private var companyScreens: ICompanyScreens
) : MvpPresenter<IMainView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        init()
    }

    private fun init() {
        router.newRootScreen(companyScreens.companyListFragment())
    }
}
