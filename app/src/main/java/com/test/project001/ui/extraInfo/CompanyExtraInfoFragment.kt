package com.test.project001.ui.extraInfo

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.squareup.picasso.Picasso
import com.test.project001.App
import com.test.project001.R
import com.test.project001.models.Company
import javax.inject.Inject
import javax.inject.Provider
import kotlinx.android.synthetic.main.company_extra_info.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter

class CompanyExtraInfoFragment : MvpAppCompatFragment(), ICompanyExtraInfoView {

    @Inject
    lateinit var presenterProvider: Provider<CompanyExtraInfoPresenter>

    @ProvidePresenter
    fun providePresenter(): CompanyExtraInfoPresenter = presenterProvider.get()

    @InjectPresenter
    lateinit var presenter: CompanyExtraInfoPresenter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.company_extra_info, null)
        val map = view.findViewById<MapView>(R.id.map)
        map.onCreate(savedInstanceState)
        map.onResume()
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val id = arguments?.getString(ID, "") ?: ""
        App.appComponent
            .provideMainActivityComponent()
            .provideCompanyExtraInfoFragmentComponent(CompanyExtraInfoFragmentModule(id))
            .inject(this)
    }

    override fun fillData(company: Company) {
        name.text = company.name
        description.text = company.description
        phoneNumber.text = company.phone
        site.text = company.www

        Picasso.with(context)
            .load(App.BASE_URL + company.img)
            .error(R.drawable.ic_placeholder_error)
            .into(image)

        if (!TextUtils.isEmpty(company.description)) {
            description.visibility = View.VISIBLE
        }

        if (!TextUtils.isEmpty(company.phone)) {
            phoneNumber.visibility = View.VISIBLE
        }

        if (!TextUtils.isEmpty(company.www)) {
            site.visibility = View.VISIBLE
        }

        if (company.lat != 0.0 && company.lon != 0.0) {
            map.visibility = View.VISIBLE
            map.getMapAsync { googleMap ->
                showPlaceOnMapIfNeed(googleMap, company)
            }
        }
    }

    private fun showPlaceOnMapIfNeed(mMap: GoogleMap, company: Company) {
        val place = LatLng(company.lat!!, company.lon!!)
        mMap.addMarker(MarkerOptions().position(place).title(company.name))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(place, 15f))
        mMap.uiSettings.isZoomGesturesEnabled = false
        mMap.uiSettings.isRotateGesturesEnabled = false
        mMap.uiSettings.isScrollGesturesEnabled = false
    }

    override fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    companion object {
        private const val ID = "id"

        fun newInstance(taskId: String): CompanyExtraInfoFragment {
            val args = Bundle().apply {
                putString(ID, taskId)
            }
            return CompanyExtraInfoFragment().apply { arguments = args }
        }
    }
}
