package com.test.project001.ui.extraInfo

import com.test.project001.models.Company
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.SkipStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface ICompanyExtraInfoView : MvpView {
    fun fillData(company: Company)

    @StateStrategyType(SkipStrategy::class)
    fun showToast(message: String)
}
