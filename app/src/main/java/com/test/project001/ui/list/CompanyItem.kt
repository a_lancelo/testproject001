package com.test.project001.ui.list

import android.view.View
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.squareup.picasso.Picasso
import com.test.project001.App
import com.test.project001.R
import kotlinx.android.synthetic.main.company_list_item.view.*

class CompanyItem(
    var text: String,
    var image: String
) : AbstractItem<CompanyItem, CompanyItemViewHolder>() {

    override fun getType(): Int {
        return R.id.textView
    }

    override fun getViewHolder(v: View): CompanyItemViewHolder {
        return CompanyItemViewHolder(v)
    }

    override fun getLayoutRes(): Int {
        return R.layout.company_list_item
    }
}

class CompanyItemViewHolder(itemView: View) : FastAdapter.ViewHolder<CompanyItem>(itemView) {

    override fun unbindView(item: CompanyItem) {
        itemView.textView.text = null
    }

    override fun bindView(item: CompanyItem, payloads: MutableList<Any>) {
        itemView.textView.text = item.text
        Picasso.with(itemView.context)
            .load(App.BASE_URL + item.image)
            .error(R.drawable.ic_placeholder_error)
            .into(itemView.imageView)
    }
}
