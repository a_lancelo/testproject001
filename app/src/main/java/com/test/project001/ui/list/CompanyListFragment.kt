package com.test.project001.ui.list

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import com.test.project001.App
import com.test.project001.R
import com.test.project001.models.Company
import javax.inject.Inject
import javax.inject.Provider
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter

class CompanyListFragment : MvpAppCompatFragment(), ICompanyListView {

    @Inject
    lateinit var presenterProvider: Provider<CompanyListPresenter>

    @ProvidePresenter
    fun providePresenter(): CompanyListPresenter = presenterProvider.get()

    @InjectPresenter
    lateinit var presenter: CompanyListPresenter

    private var component: CompanyListFragmentComponent? = null

    private lateinit var recyclerView: RecyclerView
    private val viewAdapter = FastItemAdapter<CompanyItem>()
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.company_list, null)
        viewManager = LinearLayoutManager(activity)
        recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        App.appComponent
            .provideMainActivityComponent()
            .provideCompanyListFragmentComponent()
            .inject(this)
    }

    override fun onDestroy() {
        component = null
        super.onDestroy()
    }

    override fun onDestroyView() {
        recyclerView.run {
            layoutManager = null
            adapter = null
        }
        super.onDestroyView()
    }

    override fun fillData(data: List<Company>) {
        val items = data.map {
            CompanyItem(it.name ?: "", it.img ?: "")
        }
        viewAdapter.setNewList(items).withOnClickListener { _, adapter, item, position ->
            presenter.goToExtraInfo(data[position].id!!)
            true
        }
    }
}
