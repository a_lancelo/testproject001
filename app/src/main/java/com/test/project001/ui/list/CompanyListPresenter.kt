package com.test.project001.ui.list

import com.test.project001.common.BaseMvpPresenter
import com.test.project001.models.Company
import com.test.project001.navigation.ICompanyScreens
import com.test.project001.repository.ICompanyRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState
import ru.terrakok.cicerone.Router

@InjectViewState
class CompanyListPresenter(
    private val router: Router,
    private var companyScreens: ICompanyScreens,
    private var companyRepository: ICompanyRepository
) : BaseMvpPresenter<ICompanyListView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        companyRepository.getCompaniesList()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::onListLoaded) {}
            .unsubscribeWhenDestroy()
    }

    private fun onListLoaded(data: List<Company>) {
        viewState.fillData(data)
    }

    fun goToExtraInfo(id: String) {
        router.navigateTo(companyScreens.companyExtraInfoFragment(id))
    }
}
