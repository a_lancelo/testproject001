package com.test.project001.ui.main

import com.test.project001.navigation.CompanyScreensImpl
import com.test.project001.navigation.ICompanyScreens
import com.test.project001.repository.ICompanyRepository
import com.test.project001.repository.api.Api
import com.test.project001.repository.impl.CompanyRepositoryImpl
import com.test.project001.ui.extraInfo.CompanyExtraInfoFragmentComponent
import com.test.project001.ui.extraInfo.CompanyExtraInfoFragmentModule
import com.test.project001.ui.list.CompanyListFragmentComponent
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import ru.terrakok.cicerone.Router

@Module
class MainModule() {

    @Provides
    fun provideMainPresenter(
        router: Router,
        companyScreens: ICompanyScreens
    ): MainPresenter {
        return MainPresenter(router, companyScreens)
    }

    @Provides
    fun provideCompanyScreens(): ICompanyScreens {
        return CompanyScreensImpl()
    }

    @Provides
    fun provideCompanyRepository(hostApi: Api): ICompanyRepository {
        return CompanyRepositoryImpl(hostApi)
    }
}

@Subcomponent(modules = [MainModule::class])
interface MainComponent {
    fun provideCompanyListFragmentComponent(): CompanyListFragmentComponent
    fun provideCompanyExtraInfoFragmentComponent(companyExtraInfoFragmentModule: CompanyExtraInfoFragmentModule): CompanyExtraInfoFragmentComponent
    fun inject(target: MainActivity)
}
