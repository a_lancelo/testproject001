package com.test.project001.ui.list

import com.test.project001.navigation.ICompanyScreens
import com.test.project001.repository.ICompanyRepository
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import ru.terrakok.cicerone.Router

@Module
class CompanyListFragmentModule() {

    @Provides
    fun provideCompanyListPresenter(
        router: Router,
        companyScreens: ICompanyScreens,
        companyRepository: ICompanyRepository
    ): CompanyListPresenter {
        return CompanyListPresenter(router, companyScreens, companyRepository)
    }
}

@Subcomponent(modules = [CompanyListFragmentModule::class])
interface CompanyListFragmentComponent {
    fun inject(target: CompanyListFragment)
}
