package com.test.project001.ui.main

import android.content.Context
import android.os.Bundle
import com.test.project001.App
import com.test.project001.R
import javax.inject.Inject
import javax.inject.Provider
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator

class MainActivity : MvpAppCompatActivity(), IMainView {

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @Inject
    lateinit var presenterProvider: Provider<MainPresenter>

    @ProvidePresenter
    fun providePresenter(): MainPresenter = presenterProvider.get()

    @InjectPresenter
    lateinit var presenter: MainPresenter

    private val navigator: SupportAppNavigator =
        SupportAppNavigator(this, supportFragmentManager, R.id.frameLayout)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun attachBaseContext(newBase: Context) {
        App.appComponent
            .provideMainActivityComponent()
            .inject(this)
        super.attachBaseContext(newBase)
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }
}
