package com.test.project001.ui.extraInfo

import com.test.project001.repository.ICompanyRepository
import dagger.Module
import dagger.Provides
import dagger.Subcomponent

@Module
class CompanyExtraInfoFragmentModule(private val companyId: String = "") {

    @Provides
    fun provideCompanyExtraInfoPresenter(
        companyRepository: ICompanyRepository
    ): CompanyExtraInfoPresenter {
        return CompanyExtraInfoPresenter(companyRepository, companyId)
    }
}

@Subcomponent(modules = [CompanyExtraInfoFragmentModule::class])
interface CompanyExtraInfoFragmentComponent {
    fun inject(target: CompanyExtraInfoFragment)
}
