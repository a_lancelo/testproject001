package com.test.project001.ui.extraInfo

import com.test.project001.common.BaseMvpPresenter
import com.test.project001.models.Company
import com.test.project001.repository.ICompanyRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState

@InjectViewState
class CompanyExtraInfoPresenter(
    private var companyRepository: ICompanyRepository,
    private val companyId: String
) : BaseMvpPresenter<ICompanyExtraInfoView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        companyRepository.getCompanyInfo(companyId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::onDataLoaded) {
                viewState.showToast(it.message ?: "")
            }
            .unsubscribeWhenDestroy()
    }

    private fun onDataLoaded(data: List<Company>) {
        viewState.fillData(data[0])
    }
}
