package com.test.project001.ui.list

import com.test.project001.models.Company
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface ICompanyListView : MvpView {
    fun fillData(data: List<Company>)
}
