package com.test.project001

import android.app.Application
import com.test.project001.di.AppComponent
import com.test.project001.di.AppModule
import com.test.project001.di.DaggerAppComponent
import ru.terrakok.cicerone.Cicerone

class App : Application() {
    companion object {
        const val BASE_URL: String = "http://megakohz.bget.ru/test_task/"

        lateinit var appComponent: AppComponent
            private set
    }

    override fun onCreate() {
        super.onCreate()
        val cicerone = Cicerone.create()
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(cicerone))
            .build()
    }
}
