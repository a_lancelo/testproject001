package com.test.project001.navigation

import ru.terrakok.cicerone.android.support.SupportAppScreen

interface ICompanyScreens {
    fun companyListFragment(): SupportAppScreen
    fun companyExtraInfoFragment(id: String): SupportAppScreen
}
