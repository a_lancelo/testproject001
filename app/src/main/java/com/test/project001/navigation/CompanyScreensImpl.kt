package com.test.project001.navigation

import androidx.fragment.app.Fragment
import com.test.project001.ui.extraInfo.CompanyExtraInfoFragment
import com.test.project001.ui.list.CompanyListFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class CompanyScreensImpl : ICompanyScreens {
    override fun companyListFragment(): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getFragment(): Fragment {
                return CompanyListFragment()
            }
        }
    }

    override fun companyExtraInfoFragment(id: String): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getFragment(): Fragment {
                return CompanyExtraInfoFragment.newInstance(id)
            }
        }
    }
}
