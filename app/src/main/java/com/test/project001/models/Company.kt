package com.test.project001.models

data class Company(
    val id: String?,
    val name: String?,
    val img: String?,
    val description: String?,
    val lat: Double?,
    val lon: Double?,
    val www: String?,
    val phone: String?
)
