package com.test.project001.di

import com.test.project001.ui.main.MainComponent
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class, NetworkModule::class])
@Singleton
interface AppComponent {
    fun provideMainActivityComponent(): MainComponent
}
