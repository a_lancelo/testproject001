package com.test.project001.di

import dagger.Module
import dagger.Provides
import javax.inject.Singleton
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

@Module
class AppModule(
    private val cicerone: Cicerone<Router>
) {

    @Provides
    @Singleton
    fun provideNavigatorHolder(): NavigatorHolder {
        return cicerone.navigatorHolder
    }

    @Provides
    @Singleton
    fun provideRouter(): Router {
        return cicerone.router
    }
}
