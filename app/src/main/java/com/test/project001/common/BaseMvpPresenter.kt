package com.test.project001.common

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import moxy.MvpPresenter
import moxy.MvpView

abstract class BaseMvpPresenter<View : MvpView> : MvpPresenter<View>() {
    private val onDestroyCompositeSubscription = CompositeDisposable()

    protected fun Disposable.unsubscribeWhenDestroy(): Disposable {
        unsubscribeOnDestroy(this)
        return this
    }

    protected fun unsubscribeOnDestroy(subscription: Disposable) {
        onDestroyCompositeSubscription.add(subscription)
    }

    override fun onDestroy() {
        super.onDestroy()
        onDestroyCompositeSubscription.clear()
    }
}
